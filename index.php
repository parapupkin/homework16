<?php
require 'vendor/autoload.php';

// Функция для проверки данных, полученных от пользователя
$patternAddress = "/[^\\w\\s,\\.-]/iu";
$patternСoordinates = "/[^0-9\\.-]/i";
function check_data($pattern, $value_user) 
{        
    if (preg_match($pattern, $value_user)) {
        exit("Допустимо использовать только буквы, цифры, дефис, точку и запятую!");
    }        
}

// Создаем объект для работы с Yandex\Geo
$api = new \Yandex\Geo\Api();

if (isset($_GET['address'])) {
    check_data($patternAddress, $_GET['address']);
    $api->setQuery($_GET['address']);
}

// Настройка фильтров
@$api
    ->setLimit(INF) // кол-во результатов
    ->setLang(\Yandex\Geo\Api::LANG_RU) // локаль ответа
    ->load();

$response = $api->getResponse();
$response->getFoundCount(); // кол-во найденных адресов
$response->getQuery(); // исходный запрос
$response->getLatitude(); // широта для исходного запроса
$response->getLongitude(); // долгота для исходного запроса

// Список найденных точек
$collection = $response->getList();

// Создаем переменные для js
if (isset($_GET['only_address'])) {
    check_data($patternAddress, $_GET['only_address']);
    check_data($patternСoordinates, $_GET['latitude']); 
    check_data($patternСoordinates, $_GET['longitude']); 
    $latitude = $_GET['latitude'];
    $longitude = $_GET['longitude'];
    $address = $_GET['only_address'];
    $only_address = $_GET['only_address'];
} elseif (!empty($collection)) {
    $latitude = $collection[0]->getLatitude();
    $longitude = $collection[0]->getLongitude();
    $address = $collection[0]->getAddress();
}

if (isset($_GET['address']) && $response->getFoundCount() == 0) {
    echo "По Вашему запросу ничего не найдено";
    exit();
}

require_once 'index_html.php';

