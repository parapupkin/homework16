<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Поиск по координатам</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
    <div class="container">
        <?php if (!isset($only_address)) { ?>
        <h1>Добро пожаловать в поисковую систему Вадика!</h1>
        <p class="intro">Хотите найти координаты по адресу - тогда Вам сюда!</p>
        <form action="index.php">Введите адрес для поиска
            <input class="address" type="text" name="address" placeholder="пример: Беларусь, Гомель, улица Быховская дом 113А">
            <input type="submit" value="Найти">
        </form>        
        <?php } if (isset($address) && !isset($only_address)) { ?>        
        <p>По запросу "<?= $address ?>" найдены следующие совпадения:</p>
            <table>
                <tr>
                    <th>№ п/п</th>  
                    <th>Адрес</th> 
                    <th>Широта</th>  
                    <th>Долгота</th>                  
                </tr>            
            <?php 
                $i = 1;
                foreach ($collection as $item) { ?>
                <tr>
                    <td class="number"><?=$i?></td>
                    <td><a href="index.php?latitude=<?= $item->getLatitude(); ?>&longitude=<?= $item->getLongitude(); ?>&only_address=<?= $item->getAddress(); ?>"><?= $item->getAddress(); ?></a></td>
                    <td><?= $item->getLatitude(); ?></td>
                    <td><?= $item->getLongitude(); ?></td>
                </tr>           
            <?php $i++; } ?>
            </table>
            <p>Кликните мышкой по адресу, чтобы отрисовать точку на карте</p>
         <?php } if (isset($only_address)) { ?>
        <p>На карте отображена точка по запросу "<?= $only_address ?>" - с широтой: "<?= $latitude ?>", долготой "<?= $longitude ?>".</p>        
        <?php } if (isset($latitude) && isset($longitude)) { ?>         
        <div id="map" style="width: 600px; height: 400px"></div>
        <a class="exit" href="index.php">Вернуться на главную страницу</a>
        <?php } ?>
    </div> 
    <script type="text/javascript">
        ymaps.ready(init);
        var myMap, 
            myPlacemark;

        function init(){ 
            myMap = new ymaps.Map("map", {
                center: [<?= $latitude; ?>, <?= $longitude; ?>],
                zoom: 10
            }); 
            
            myPlacemark = new ymaps.Placemark([<?= $latitude; ?>, <?= $longitude; ?>], {
                hintContent: '<?= $address; ?>',
                balloonContent: '<?= $address; ?>'
            });
            
            myMap.geoObjects.add(myPlacemark);
        }
    </script>

</body>
</html>